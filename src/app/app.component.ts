import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  triggerRangeError() {
    throw new RangeError('Fake range error');
  }

  triggerURIError() {
    decodeURIComponent('%');
  }

  triggerSyntaxError() {
    JSON.parse('this triggers a syntax error');
  }

}
